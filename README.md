# Fetch Shoe Prices

This project is used as a separate API handler, that will give a random shoe price.
You can assume this project as a separate microservice, written in NodeJS.

## Getting started

To run this project, please install `npm` & `node` on your machine.

1. Run `npm install`
2. Run `npm start`
3. The project should run on port 9090. To verify, please cURL the request (url given below) and ensure you get a valid response.

## API Details

Once the project starts on port 9090, you will be able to use below API:

URL: `http://localhost:9090/fetch-random-price/1`

Response:
```
{
 "shoePrice" : 177
}
```

const express = require('express');
const fetch = require('node-fetch');
const cors = require('cors');

const app = express();
const port = 9090;

app.use(cors());

app.get('/fetch-random-price/:id', (req, res) => {
    const price = Math.floor(Math.random() * (200-100 + 1)) + 100;
    res.send(JSON.stringify({shoePrice: price}));
});

app.listen(port, () => {
    console.log(`Started server for fetching random prices at ${port}`);
});
